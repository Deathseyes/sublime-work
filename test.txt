
#define LowTemProte_Min          5        //
#define LowTemProte_Max          11      //10度，11表示关闭低温保护功能

#define HigTemProte_Min          34      //35度，34表示关闭高温保护功能
#define HigTemProte_Max          70      
//EEPROM地址
#define FUN_EEPROM_ADD          0x00    //功能码首地址默认0x00，修改无效
enum 
{
    LOGIC_ADD = 0,          //逻辑状态地址 
    SetTemperMax_ADD,
    SetTemperMin_ADD,
    RoomLowTemperProMod_ADD,
    RoomHighTemperProMod_ADD,
    RoomHighTemperPro_ADD,
    WaterLowTemperProMod_ADD, 
    WaterTemperPro_ADD,
    PumpABSMod_ADD,
    TemStartWave_ADD,       //温度差地址     ss
    HandSetTemper_ADD,          
    Work_Mode_ADD,
    FUN_CODE_COUNT
};

#define PRO_EEPROM_ADD          0x10    //编程码首地址
//==========================================================
//#pragma rambank0
//功能码
#define     FUN_CODE_NUM    FUN_CODE_COUNT
MAIN_EXT    INT8U           Giv_FunCode[FUN_CODE_NUM];
#define     Giv8U_LogicState        Giv_FunCode[LOGIC_ADD]      //开关机状态
#define     SetTemperMax            Giv_FunCode[SetTemperMax_ADD]        
